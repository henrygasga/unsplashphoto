package com.henry.unsplash.network

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import androidx.appcompat.widget.AppCompatImageView
import java.io.InputStream
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class DownloadPhotosTask(imgView: AppCompatImageView, imgUrl: String) : AsyncTask<Void, Void, Bitmap?>() {

    @SuppressLint("StaticFieldLeak")
    var imgThumbnail: AppCompatImageView = imgView
    var imgUrlThumb: String = imgUrl

    override fun onPostExecute(result: Bitmap?) {
        super.onPostExecute(result)
        imgThumbnail.setImageBitmap(result)
    }

    override fun doInBackground(vararg params: Void?): Bitmap? {

        try {
            val bmp: Bitmap
            val photoUrl = URL(imgUrlThumb)
            val conn = photoUrl.openConnection() as HttpURLConnection
            val inStream: InputStream = conn.inputStream
            bmp = BitmapFactory.decodeStream(inStream)
            if (bmp != null) {
                return bmp
            }
        } catch (e: Exception) {
            Log.d("onBindViewHolder","ERROR: $e")
        }
        return null
    }
}