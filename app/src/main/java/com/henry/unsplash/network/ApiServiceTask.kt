package com.henry.unsplash.network

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.henry.unsplash.models.*
import com.henry.unsplash.utils.Constant
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.net.UnknownHostException


class ApiServiceTask(mContext: Context, mListener: OnLoadListener) : AsyncTask<Void, Void, ArrayList<ImageResponse>?>() {

    @SuppressLint("StaticFieldLeak")
    var context: Context = mContext
    var listerner: OnLoadListener = mListener

    override fun onPostExecute(result: ArrayList<ImageResponse>?) {
        super.onPostExecute(result)
        listerner.onLoadComplete(result)
    }

    override fun onPreExecute() {
        super.onPreExecute()
        listerner.onLoadStart()
    }

    override fun doInBackground(vararg params: Void?): ArrayList<ImageResponse>? {
        var reader: BufferedReader? = null
        try {
            val photoUrl = URL("${Constant.BASE_URL}${Constant.API_PHOTOS}")
            val conn = photoUrl.openConnection() as HttpURLConnection
            conn.requestMethod = Constant.GET_REQUEST
            conn.setRequestProperty(Constant.AUTHORIZATION,"Client-ID ${Constant.CLIENT_ID}")
            conn.connect()

            if (conn.responseCode != 200) {
                return null
            }
            val inputStream = conn.inputStream ?: return null

            reader = BufferedReader(InputStreamReader(inputStream))

            val line: String = reader.readLine()

            return serviceResponse(line)

        } catch (e: UnknownHostException) {
            e.printStackTrace()
            return null
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        } finally {
            if (reader != null) {
                try {
                    reader.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }

    fun serviceResponse(res: String): ArrayList<ImageResponse> {
        var response: ImageResponse? = null
        var mylist: List<Any> = arrayListOf()
        var imageList: ArrayList<ImageResponse> = arrayListOf()
        try {
            val dataJson = JSONArray(res)
            val anyType:Any?
            val resArray:List<ImageResponse> = arrayListOf()
            for (i in 0 until dataJson.length()) {
                val jObj = dataJson.getJSONObject(i)
                val linksObj = jObj.getJSONObject("links")
                val urlsObj = jObj.getJSONObject("urls")
                val userObj = jObj.getJSONObject("user")
                val userLinkObj = userObj.getJSONObject("links")
                val userProfileImgObj = userObj.getJSONObject("profile_image")

                var linkObj = Links(
                    linksObj.getString("download"),
                    linksObj.getString("download_location"),
                    linksObj.getString("html"),
                    linksObj.getString("self")
                )
                val urlData = Urls(
                    urlsObj.getString("full"),
                    urlsObj.getString("raw"),
                    urlsObj.getString("regular"),
                    urlsObj.getString("small"),
                    urlsObj.getString("thumb")
                )
                val linkX = LinksX(
                    userLinkObj.getString("followers"),
                    userLinkObj.getString("following"),
                    userLinkObj.getString("html"),
                    userLinkObj.getString("likes"),
                    userLinkObj.getString("photos"),
                    userLinkObj.getString("portfolio"),
                    userLinkObj.getString("self")
                )
                val profileImage = ProfileImage(
                    userProfileImgObj.getString("large"),
                    userProfileImgObj.getString("medium"),
                    userProfileImgObj.getString("small")
                )
                val userData = User(
                    userObj.getBoolean("accepted_tos"),
                    userObj.getString("bio"),
                    userObj.getString("first_name"),
                    userObj.getString("id"),
                    userObj.getString("instagram_username"),
                    userObj.getString("last_name"),
                    linkX,
                    "",
                    userObj.getString("name"),
                    userObj.getString("portfolio_url"),
                    profileImage,
                    userObj.getInt("total_collections"),
                    userObj.getInt("total_likes"),
                    userObj.getInt("total_photos"),
                    "",
                    userObj.getString("updated_at"),
                    userObj.getString("username")
                )

                imageList.add(ImageResponse(
                    jObj.getString("alt_description"),
                    mylist,
                    jObj.getString("color"),
                    jObj.getString("created_at"),
                    mylist,
                    jObj.getString("description"),
                    jObj.getInt("height"),
                    jObj.getString("id"),
                    jObj.getBoolean("liked_by_user"),
                    jObj.getInt("likes"),
                    linkObj,
                    jObj.getString("promoted_at"),
                    jObj.getString("updated_at"),
                    urlData,
                    userData,
                    jObj.getInt("width")
                ))
            }
        }catch (e: Exception) {
            listerner.onFailed()
        }

        return imageList
    }

    interface OnLoadListener {
        fun onLoadStart()
        fun onLoadComplete(result: ArrayList<ImageResponse>?)
        fun onFailed()
    }
}