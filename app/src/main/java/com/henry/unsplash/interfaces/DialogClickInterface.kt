package com.henry.unsplash.interfaces

interface DialogClickInterface {
    fun onPositiveButtonClick()
    fun onNegativeButtonClick()
}