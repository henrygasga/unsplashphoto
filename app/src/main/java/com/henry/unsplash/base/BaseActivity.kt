package com.henry.unsplash.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.henry.unsplash.R
import com.henry.unsplash.interfaces.DialogClickInterface
import com.henry.unsplash.utils.ActivityType
import com.henry.unsplash.utils.DialogFactory
import com.henry.unsplash.views.activities.MainActivity

open class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun launchActivity(act: Activity, moveTo: ActivityType) {
        var intent = Intent(act, MainActivity::class.java)
        when(moveTo) {
            ActivityType.HOME -> {
                // do nothing
            }
            ActivityType.IMAGE_VIEW -> {
                // do nothing
            }
        }
        act.startActivity(intent)
//        finish()
    }

    fun showToast(msg: String) {
        Toast.makeText(baseContext, msg,Toast.LENGTH_LONG).show()
    }

    fun showErrorDialog() {
        DialogFactory.createOkErrorDialog(
            baseContext,
            getString(R.string.failed_request),
            object : DialogClickInterface {
                override fun onPositiveButtonClick() {

                }

                override fun onNegativeButtonClick() {

                }
            }
        ).show()
    }
}