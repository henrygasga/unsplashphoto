package com.henry.unsplash.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.henry.unsplash.R
import com.henry.unsplash.adapters.ImageAdapter
import com.henry.unsplash.base.BaseActivity
import com.henry.unsplash.interfaces.DialogClickInterface
import com.henry.unsplash.models.ImageResponse
import com.henry.unsplash.network.ApiServiceTask
import com.henry.unsplash.utils.DialogFactory
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : BaseActivity(), ApiServiceTask.OnLoadListener {

    var itemList: List<ImageResponse> = arrayListOf()
    var dialogLoading: AppCompatDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dialogLoading = DialogFactory.createProgressDialog(this)
        val apiService = ApiServiceTask(this,this)
        apiService.execute()
    }

    override fun onLoadComplete(result: ArrayList<ImageResponse>?) {
        dialogLoading?.dismiss()
        if(result!!.size > 0) {
            rvPhotos.apply {
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = ImageAdapter(
                    this@MainActivity,
                    items = result,
                    ls = object : ImageAdapter.ItemClickInterface {
                        override fun onItemClick(v: View?, item: ImageResponse, position: Int) {

                        }
                    }
                )
            }
        } else {
            showErrorDialog()
        }
    }

    override fun onLoadStart() {
        dialogLoading?.show()
    }

    override fun onFailed() {
        dialogLoading?.dismiss()
        showErrorDialog()
    }
}
