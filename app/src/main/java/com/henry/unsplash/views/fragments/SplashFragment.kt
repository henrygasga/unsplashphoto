package com.henry.unsplash.views.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.henry.unsplash.R
import com.henry.unsplash.base.BaseFragment
import com.henry.unsplash.views.activities.SplashActivity

class SplashFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_splash,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Handler().postDelayed({
            (activity as SplashActivity).moveToLogin()
        }, 2000)
    }
}