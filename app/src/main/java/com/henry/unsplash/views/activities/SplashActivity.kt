package com.henry.unsplash.views.activities

import android.os.Bundle
import com.henry.unsplash.R
import com.henry.unsplash.base.BaseActivity
import com.henry.unsplash.utils.ActivityType

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    fun moveToLogin() {
        launchActivity(this, ActivityType.HOME)
        finish()
    }
}