package com.henry.unsplash.utils

enum class ActivityType {
    HOME,
    IMAGE_VIEW
}