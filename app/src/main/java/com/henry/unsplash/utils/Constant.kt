package com.henry.unsplash.utils

class Constant {
    companion object {
        val AUTHORIZATION = "Authorization"
        val CLIENT_ID = "08fc548aa1bbb22d2f15921e13bf5f28a466b5e313fb9da9972f1524d47a9a25"
        val GET_REQUEST = "GET"
        val BASE_URL = "https://api.unsplash.com/"
        val API_PHOTOS = "photos"
    }
}