package com.henry.unsplash.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import com.henry.unsplash.R
import com.henry.unsplash.interfaces.DialogClickInterface

class DialogFactory {
    companion object {
        @JvmStatic
        fun createProgressDialog(
            context: Context
        ): AppCompatDialog {

            val v: View = LayoutInflater.from(context).inflate(R.layout.loading_layout, null)
            val dialog = AppCompatDialog(context)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setContentView(v)
            return dialog
        }

        @JvmStatic
        fun createOkErrorDialog(context: Context, message: String, clickListener: DialogClickInterface) : AppCompatDialog {
            val v: View = LayoutInflater.from(context).inflate(R.layout.dialog_error, null)
            val dialog = AppCompatDialog(context)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)

            val btnOk = v.findViewById<View>(R.id.btn_ok_dialog) as  AppCompatButton
            val textMessage = v.findViewById<View>(R.id.mtv_dialog_message) as AppCompatTextView

            textMessage.text = message
            btnOk.setOnClickListener {
                dialog.dismiss()
                clickListener.onPositiveButtonClick()
            }

            dialog.setContentView(v)
            return dialog
        }
    }
}