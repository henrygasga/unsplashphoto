package com.henry.unsplash.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.henry.unsplash.R
import com.henry.unsplash.models.ImageResponse
import androidx.appcompat.widget.AppCompatImageView
import com.henry.unsplash.network.DownloadPhotosTask
import com.henry.unsplash.views.activities.MainActivity


class ImageAdapter(context: Context,  items: List<ImageResponse>, ls: ItemClickInterface):
    RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {

    var itemList: List<ImageResponse> = items
    var listener: ItemClickInterface = ls
    var aContext: Context = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.item_image_rv, parent, false)
        return ImageViewHolder(v)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val item: ImageResponse = itemList[position]
        (aContext as MainActivity).runOnUiThread {
            if(!item.urls.thumb.isEmpty()) {
                DownloadPhotosTask(holder.imgThumbHolder, item.urls.thumb).execute()
            }
        }


        holder.textView.text = item.user.name
        holder.textViewSub.text = item.user.location.toString()
    }

    inner class ImageViewHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        var view: View? = v

        init {
            view?.setOnClickListener(this)
        }
        var textView: TextView = v.findViewById(R.id.tvName)
        var textViewSub: TextView = v.findViewById(R.id.tvNameSub)
        var imgThumbHolder: AppCompatImageView = v.findViewById(R.id.imgThumb)
        override fun onClick(v: View?) {

        }
    }

    interface ItemClickInterface {
        fun onItemClick(v:View?, item: ImageResponse, position: Int)
    }
}